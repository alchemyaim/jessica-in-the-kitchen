<div class="home-post section">
	<div class="wrapper">
		<div class="postimgcol">
		<?php if ( get_field('post_primary_img1') && get_field('post_primary_img2')) : ?>
			<a class="swapimgs" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php
				$image_object = get_field('post_primary_img1');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
				<?php
				$image_object = get_field('post_primary_img2');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
			</a>
		<?php else: ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>			
		<?php endif; ?>
		</div>
		<div class="posttxtcol">
			<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="postcats">
				<?php $categories = get_the_category();
				$separator = ' ›› ';
				$output = '';
				if ( ! empty( $categories ) ) {
				    foreach( $categories as $category ) {
				        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
				    }
				    echo trim( $output, $separator );
				} ?>
			</div>
			<?php if(get_field('post_custom_excerpt')): ?>
			<div class="excerpt"><?php the_field('post_custom_excerpt'); ?></div>
			<?php else: ?>
			<div class="excerpt"><?php the_excerpt(''); ?></div>
			<?php endif; ?>
			<div class="recipe-meta">
				<?php if(get_field('recipe_preparation')): ?><div>Preparation: <?php the_field('recipe_preparation'); ?></div><?php endif; ?>
				<?php if(get_field('recipe_cook_time')): ?><div>Cook Time: <?php the_field('recipe_cook_time'); ?></div><?php endif; ?>
				<?php if(get_field('recipe_serves')): ?><div>Serves: <?php the_field('recipe_serves'); ?></div><?php endif; ?>
			</div>
			<a class="view-post" href="<?php the_permalink(); ?>">View the Post</a>

	<?php if(have_rows('recipe_ingredients')): ?>
	<div class="ingredients">
		<div class="ingr-title">Ingredients</div>
		<?php while(have_rows('recipe_ingredients')): the_row(); ?>
			<div><strong><?php the_sub_field('ingredient_title'); ?></strong></div>
			<?php if(have_rows('ingredient_list')): ?>
			<ul class="ingredient-list">
				<?php while(have_rows('ingredient_list')): the_row(); ?>
				<li><?php the_sub_field('ingredient_list_text'); ?></li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>


		</div>
	</div>

	</div>
</div>