<?php
/**
 * The template for displaying archive pages.
 */

get_header();
?>

<div class="shopwrap section">
	<div class="wrapper">
		<h2 class="page-title"><?php single_term_title(); ?></h2>

		<div class="shoppostlist section">

	<?php
	if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); ?>

	<div class="shoppost">
		<a href="<?php the_field('shop_prod_link'); ?>">
			<div class="imgbox">
				<?php
				$image_object = get_field('shop_prod_photo');
				$image_size = 'medium';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
			</div>
			<h2><?php the_title(); ?></h2>
			<div><?php the_field('shop_prod_desc'); ?></div>
		</a>
	</div>


		<?php endwhile;

		the_posts_navigation();

	else :

		get_template_part( '_template-parts/content', 'none' );

	endif; ?>

</div><!-- .content-area -->

<?php get_footer(); ?>
