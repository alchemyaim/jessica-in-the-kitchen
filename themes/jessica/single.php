<?php
/**
 * The template for displaying all single posts.
 */

get_header();
?>


<?php
while ( have_posts() ) : the_post(); ?>


	<div class="post-banner-imgs section">
		<div class="wrapper">
		<?php if ( get_field('post_primary_img1') && get_field('post_primary_img2')) : ?>
			<div class="two-col-imgs section">
				<?php
				$image_object = get_field('post_primary_img1');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
				<?php
				$image_object = get_field('post_primary_img2');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
			</div>
		<?php else: ?>
			<div class="single-img section">
				<?php the_post_thumbnail(); ?>
			</div>
		<?php endif; ?>
		</div>
	</div>
	<div class="post-header">
		<div class="wrapper">
			<div class="date"><?php the_time('F j, Y'); ?></div>
			<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="postcats">
				<?php $categories = get_the_category();
				$separator = ' ›› ';
				$output = '';
				if ( ! empty( $categories ) ) {
				    foreach( $categories as $category ) {
				        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
				    }
				    echo trim( $output, $separator );
				} ?>
			</div>
		</div>
	</div>
	
	<div class="post-main post-main-first section">
		<div class="wrapper">
			<?php the_content(); ?>

			<?php if( have_rows('post_additional_content') ): ?>
			<?php while ( have_rows('post_additional_content') ) : the_row(); ?>
			
				<?php if( get_row_layout() == 'post_sponsor_note' ): ?>
					<div class="sponsor-info section">
						<div><?php the_sub_field('post_sponsor_note_text'); ?></div>
					</div>
		        <?php elseif( get_row_layout() == 'post_standard_content' ): ?>
		        	<div class="section">
						<?php the_sub_field('post_standard_wysiwyg'); ?>
		        	</div>
		        <?php endif; ?>
		        
		    <?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>

	<?php if(have_rows('recipe_materials')): ?>
	<div class="recipe-materials section">
		<div class="wrapper">
			<div class="materials-title section">what you’ll need</div>
			<div class="materials-list section">
				<?php while(have_rows('recipe_materials')): the_row(); ?>
				<div>
					<a href="<?php the_sub_field('recipe_material_link'); ?>">
						<?php
						$image_object = get_sub_field('recipe_material_image');
						$image_size = 'thumbnail';
						$image_url = $image_object['sizes'][$image_size];
						?>
						<img src="<?php echo $image_url; ?>">
					</a>
				</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>


	<?php if(has_category( 'recipes' )): ?>
	<div class="post-recipe section">
		<div class="wrapper">
			<div class="recipe-2col section">
				<div class="recipe-col1">
					<div class="recipe-title-area section">
						<div class="post-recipe-img">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="post-recipe-title">
							<?php if(get_field('recipe_title')): ?>
								<h2 class="page-title"><?php the_field('recipe_title'); ?></h2>
							<?php else: ?>
								<h2 class="page-title"><?php the_title(); ?></h2>
							<?php endif; ?>
						</div>
					</div>
					<div class="recipe-meta">
						<?php if(get_field('recipe_preparation')): ?><div>Preparation: <?php the_field('recipe_preparation'); ?></div><?php endif; ?>
						<?php if(get_field('recipe_cook_time')): ?><div>Cook Time: <?php the_field('recipe_cook_time'); ?></div><?php endif; ?>
						<?php if(get_field('recipe_serves')): ?><div>Serves: <?php the_field('recipe_serves'); ?></div><?php endif; ?>
						<?php if(get_field('recipe_calories')): ?>
							<div>Calories: <?php the_field('recipe_calories'); ?>


				<?php if(have_rows('recipe_facts')): ?>
				<a href="#" data-featherlight="#nutritionlightbox">Get Nutrition Facts</a>
				<div id="nutritionlightbox">
					<div class="ingredients">
						<div class="ingr-title">Nutrition Facts</div>
						<?php while(have_rows('recipe_facts')): the_row(); ?>
							<div><strong><?php the_sub_field('recipe_facts_title'); ?></strong></div>
							<?php if(have_rows('recipe_nutrition_list')): ?>
							<ul class="ingredient-list">
								<?php while(have_rows('recipe_nutrition_list')): the_row(); ?>
								<li><?php the_sub_field('recipe_nutrition_text'); ?></li>
								<?php endwhile; ?>
							</ul>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				</div>
				<?php endif; ?>



							</div>
						<?php endif; ?>
					</div>
					
					<div class="print-share">
						<a class="print" href="javascript:window.print()">Print</a>
						<div class="sharewrap">
							<span class="sharetoggle">Share</span>
							<div class="sharetogglebox">
								<?php echo do_shortcode( '[easy-social-share buttons="facebook,twitter,google,pinterest,yummly" counters=0 style="button_name" template="21" nospace="yes" point_type="simple"]' ); ?>
							</div>
						</div>
					</div>
					
					<?php if(have_rows('recipe_ingredients')): ?>
					<div class="ingredients">
						<div class="ingr-title">Ingredients</div>
						<?php while(have_rows('recipe_ingredients')): the_row(); ?>
							<div><strong><?php the_sub_field('ingredient_title'); ?></strong></div>
							<?php if(have_rows('ingredient_list')): ?>
							<ul class="ingredient-list">
								<?php while(have_rows('ingredient_list')): the_row(); ?>
								<li><?php the_sub_field('ingredient_list_text'); ?></li>
								<?php endwhile; ?>
							</ul>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="recipe-col2">
					<?php if(have_rows('recipe_instruction')): ?>
					<div class="instruction-steps">
						<div class="ingr-title">Instructions</div>
						<?php while(have_rows('recipe_instruction')): the_row(); ?>
							<div class="instruction-group">
								<div><strong><?php the_sub_field('instruction_title'); ?></strong></div>
								<?php if(have_rows('instructions_list')): ?>
								<ul class="instruction-list">
									<?php while(have_rows('instructions_list')): the_row(); ?>
									<li><?php the_sub_field('instruction_list_text'); ?></li>
									<?php endwhile; ?>
								</ul>
							</div>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php if(get_field('recipe_extra_content')): ?>
			<div class="recipe-note section">
				<?php the_field('recipe_extra_content'); ?>
			</div>
			<?php endif; ?>
			<div class="signature">by Jessica</div>
		</div>
	</div>
	<?php else : ?>
	<?php endif; ?><!-- if recipes category -->

	<div class="post-nav section">
		<div class="wrapper">
		    <?php the_post_navigation( array(
		            'prev_text' => __( '‹‹ <span>Older Posts</span>' ),
		            'next_text' => __( '<span>Newer Posts</span> ››' ),
		        ) ); ?>
		</div>
	</div>
	
	<div class="alsolike section">
		<div class="wrapper">
			<div class="section-title">You Might Also Like</div>
			<div class="also-list section">
				<?php query_posts(array('showposts'=> 3,'post__not_in' => array($post->ID))); ?>
				<?php while (have_posts()) : the_post(); ?>
				<div>
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail(array(655, 985)); ?>
						<div class="alsohover">
							<div>
								<h3><?php the_title(); ?></h3>
								<span>Read More</span>
							</div>
						</div>
					</a>
				</div>
				<?php endwhile; 
				wp_reset_query();
				?>
			</div>
		</div>
	</div>





    <?php if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif; ?>

<?php endwhile; ?>

<?php get_footer(); ?>
