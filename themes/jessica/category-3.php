<?php
/**
 * The template for displaying archive pages.
 */

get_header();
?>

<div class="catwrap catwrap-guides section">
	<div class="wrapper">
		<h2 class="page-title"><?php single_term_title(); ?></h2>
		<div class="section">
			<div class="catsidefilter">
				<div class="filterbytitle">Filter By:</div>
				<?php echo do_shortcode( '[searchandfilter id="209"]' ); ?>
			</div>
			<div class="catpostlist">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();
						get_template_part( '_template-parts/content', get_post_format() );
					endwhile;
					the_posts_navigation();
				else :
				endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>