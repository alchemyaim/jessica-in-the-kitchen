<?php
/**
 * The template for displaying search results pages.
 */

get_header();
?>

<div class="container">


<div class="catwrap section">
	<div class="wrapper">
		<h2 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'test' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		<div class="section">
			<div class="catpostlist">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();
						get_template_part( '_template-parts/content', get_post_format() );
					endwhile;
				else :
					get_template_part( '_template-parts/content', 'none' );
				endif; ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
