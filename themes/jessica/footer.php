<div class="instawrap">
	<div class="wrapper">
		<h5><?php the_field('instagram_title', 'options'); ?></h5>
		<?php echo do_shortcode( '[instagram-feed]' ); ?>
		<div class="instatag">follow  <a target="_blank" href="http://instagram.com/jessicainthekitchen/">@jessicainthekitchen</a></div>
	</div>
</div>

<footer class="site-footer section">
	<div class="footer-logonav section">
		<div class="wrapper">
			<a class="icon-logo" href="<?php echo home_url(); ?>">
				<img class="icon" src="<?php the_field('main_icon', 'options'); ?>">
				<img class="logo" src="<?php the_field('main_logo', 'options'); ?>">
			</a>
			<?php wp_nav_menu( array(
				'theme_location'  => 'footer',
				'menu' 			  => get_post_meta( $post->ID, 'meta_box_menu_name_set', true),
				'container'       => 'none',
				'container_class' => 'menu-footer',
				'container_id'    => '',
				'menu_class'      => 'nav',
				'menu_id'         => 'main-footer',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 3,
				) ); ?>
		</div>
	</div>
	<div class="copyright section">
		<div class="wrapper">
			&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> • <span>Site design by <a href="http://katelyncalautti.com/" target="_blank">KC</a> dev by <a href="http://alchemyandaim.com" target="_blank">A + A</a></span>
		</div>
	</div>
	<img class="large-icon" src="<?php the_field('main_icon', 'options'); ?>">
</footer>


<?php if(have_rows('social_links', 'options')): ?>
<div class="social-float">
	<?php while(have_rows('social_links', 'options')): the_row(); ?>
	<a href="<?php the_sub_field('social_link'); ?>"><i class="fa <?php the_sub_field('social_media_icon'); ?>" aria-hidden="true"></i></a>
	<?php endwhile; ?>
</div>
<?php endif; ?>
</div>
<?php wp_footer(); ?>
</body>
</html>