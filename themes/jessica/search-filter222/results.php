<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2014 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */
?>
<div class="catwrap section">
	<div class="wrapper">
		<h2 class="page-title"><?php single_term_title(); ?></h2>
		<div class="section">
			<div class="catsidefilter">
				<div class="filterbytitle">Filter By:</div>
				<?php if( is_category( 2 ) || cat_is_ancestor_of(2, $cat) ): ?>
				<?php echo do_shortcode( '[searchandfilter id="199"]' ); ?>
				<?php elseif(is_category( 3 )): ?>
				<?php echo do_shortcode( '[searchandfilter id="209"]' ); ?>
				<?php endif; ?>
			</div>
			<div class="catpostlist">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post();
						get_template_part( '_template-parts/content', get_post_format() );
					endwhile;
					the_posts_navigation();
				else :
				endif; ?>
			</div>
		</div>
	</div>
</div>
