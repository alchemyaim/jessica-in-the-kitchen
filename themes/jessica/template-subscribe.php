<?php
/**
 * Template Name: Subscribe Page
 */

get_header();
?>

<div class="subscribewrap section">
	<div class="wrapper">
    <?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>
	</div>
</div>
<?php get_footer(); ?>
