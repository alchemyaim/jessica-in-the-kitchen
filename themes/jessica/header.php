<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.png">

	<title><?php wp_title('|',1,'right'); ?></title>
	
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:900" rel="stylesheet">
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/_static/js/browser-detect.js"></script>
	<script>
	  (function(d) {
	    var config = {
	      kitId: 'ekz4iek',
	      scriptTimeout: 3000,
	      async: true
	    },
	    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
	  })(document);
	</script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
		.mainbar li.homelink a:after {background-image: url(<?php the_field('main_icon', 'options'); ?>)}
	</style>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/_static/styles/css/style.css" />

	<!-- For use with browser-detect.js -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/_static/styles/browser-specific-styles.css" />

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<?php wp_enqueue_script("jquery"); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class('page-' . $post->post_name); ?>>
	<div id="body-wrap">
		<nav id="sidebar-nav">
			<a href="#" id="nav-close"><div class="line1"></div><div class="line2"></div></a>
			<div class="inner">
				<?php wp_nav_menu( array(
					'theme_location'  => 'mobile',
					'menu' 			  => get_post_meta( $post->ID, 'meta_box_menu_name_set', true),
					'container'       => 'none',
					'container_class' => 'menu-mobile',
					'container_id'    => '',
					'menu_class'      => 'nav',
					'menu_id'         => 'mobilenav',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 3,
					) ); ?>
			</div>
		</nav>
		<header class="header section" role="banner">
			<div class="mainbar section">
				<a href="#" id="nav-trigger"><div class="line1"></div><div class="line2"></div><div class="line3"></div></a>
				<?php wp_nav_menu( array(
					'theme_location'  => 'topleft',
					'menu' 			  => get_post_meta( $post->ID, 'meta_box_menu_name_set', true),
					'container'       => 'none',
					'container_class' => 'menu-left',
					'container_id'    => '',
					'menu_class'      => 'nav',
					'menu_id'         => 'main-left',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 3,
					) ); ?>
					
				<?php wp_nav_menu( array(
					'theme_location'  => 'topcenter',
					'menu' 			  => get_post_meta( $post->ID, 'meta_box_menu_name_set', true),
					'container'       => 'none',
					'container_class' => 'menu-center',
					'container_id'    => '',
					'menu_class'      => 'nav',
					'menu_id'         => 'main-center',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 3,
					) ); ?>
					
				<?php wp_nav_menu( array(
					'theme_location'  => 'topright',
					'menu' 			  => get_post_meta( $post->ID, 'meta_box_menu_name_set', true),
					'container'       => 'none',
					'container_class' => 'menu-right',
					'container_id'    => '',
					'menu_class'      => 'nav',
					'menu_id'         => 'main-right',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 3,
					) ); ?>
			</div>
			<div class="section searchformwrap">
			<form class="search searchmain" method="get" action="<?php echo home_url(); ?>" role="search">
				<div class="wrapper">
					<input class="search-input" type="search" name="s" placeholder="<?php _e( 'enter keywords...', 'html5blank' ); ?>">
					<input type="hidden" value="post" name="post_type">
					<button class="search-submit" type="submit" role="button"><?php _e( 'Submit', 'html5blank' ); ?></button>
				</div>
			</form>
			</div>
			
			<div class="mainlogo section">
				<div class="wrapper">
					<a href="<?php echo home_url(); ?>">
						<img class="logo" src="<?php the_field('main_logo', 'options'); ?>">
					</a>
					<img class="tagline" src="<?php the_field('tagline', 'options'); ?>">
				</div>
			</div>
		</header>