<?php
/**
 * Template Name: About Page
 *
 */ get_header();
?>

<div class="about-banner section">
	<div class="wrapper">
		<img src="<?php the_field('about_banner'); ?>">
	</div>
</div>

<div class="about-main section">
	<img class="large-icon" src="<?php the_field('main_icon', 'options'); ?>">
	<div class="wrapper">
		<div class="about-quote page-title section">
			<?php the_field('about_quote'); ?>
		</div>
		<div class="about-title section">
			<?php the_field('about_title'); ?>
		</div>
		<div class="about-text section">
			<div class="text-col col-left">
				<?php the_field('about_text_col1'); ?>
			</div>
			<div class="text-col col-right">
				<?php the_field('about_text_col2'); ?>
			</div>
			<div class="signature">love Jessica</div>
		</div>
	</div>
</div>
<div class="about-press section">
	<div class="wrapper">
		<div class="about-title section">
			<?php the_field('press_title'); ?>
		</div>
		<?php if(have_rows('press_list')): ?>
		<div class="press-list section">
			<?php while(have_rows('press_list')): the_row(); ?>
			<div>
				<img src="<?php the_sub_field('press_list_logo'); ?>">
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</div>
<div class="about-faq section">
	<div class="wrapper">
		<div class="about-title section">
			<?php the_field('faq_title'); ?>
		</div>
		<div class="about-text section">
			<?php if(have_rows('faq_list_1')): ?>
			<div class="text-col faq-col-left">
				<?php while(have_rows('faq_list_1')): the_row(); ?>
				<div>
					<h3><?php the_sub_field('faq_1_question'); ?></h3>
					<div><?php the_sub_field('faq_1_answer'); ?></div>
				</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
			<?php if(have_rows('faq_list_2')): ?>
			<div class="text-col faq-col-right">
				<?php while(have_rows('faq_list_2')): the_row(); ?>
				<div>
					<h3><?php the_sub_field('faq_2_question'); ?></h3>
					<div><?php the_sub_field('faq_2_answer'); ?></div>
				</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>


<?php get_footer(); ?>