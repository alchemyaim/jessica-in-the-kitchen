��          �       \      \     ]  *   n  X   �  B   �     5  Y   G     �  &   �     �  9   �  ;   2  V   n  T   �       C   5  "   y     �  P   �  .   �  �  ,     �  /   �  d     Q   z     �  `   �     H  .   _     �  Y   �  2   	  Q   :	  Z   �	  )   �	  5   
     G
     f
  h   w
  1   �
    (expected: 200) A new version of this plugin is available. Can't to read the Version header for '%s'. The filename is incorrect or is not a plugin. Can't to read the plugin header for '%s'. The file does not exist. Check for updates Failed to parse plugin metadata. Try validating your .json file with http://jsonlint.com/ HTTP response code is  Plugin directory successfully renamed. Renaming %s to %s&#8230; Skipping update check for %s - installed version unknown. The URL %s does not point to a valid plugin metadata file.  The directory structure of the update is incorrect. All plugin files should be inside  The plugin metadata file does not contain the required 'name' and/or 'version' keys. This plugin is up to date. Unable to rename the update to match the existing plugin directory. Unknown update checker status "%s" WP HTTP error:  a directory named <span class="code">%s</span>, not at the root of the ZIP file. wp_remote_get() returned an unexpected result. Project-Id-Version: ACF Admin Flexible Content Collapse
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-07 12:46+0000
PO-Revision-Date: 2016-11-07 12:46+0000
Last-Translator: thomas <thomas.meyer@dreihochzwo.de>
Language-Team: German
Language: de-DE
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/  (erwartet: 200) Eine neuere Version des Plugins ist verfügbar. Kann die Version im Header für '%s' nicht finden. Der Dateiname ist falsch oder es ist kein Plugin. Kann die Version im Header für '%s' nicht finden. Die Datei ist nicht vorhanden. Nach Aktualisierung suchen Fehler beim Auslesen der Plugin Metadaten. Überprüfe die JSON-Datei über http://jsonlint.com/ TTP Response Code ist  Der Plugin-Ordner wurde erfolgreich umbenannt. Umbenennen von %s zu %s&#8230; ktualisierung kann nicht ausgeführt werden für %s - installierte Version ist unbekannt. %s ist enthält keine gültigen Plugin Metadaten.  Die Struktur des Updates ist nicht korrekt. Alle Plugin-Dateien müssen innerhalb In der Plugin Metadaten-Datei fehlen die notwendigen 'name' und/oder 'version' Schlüssel. Das Plugin ist auf dem aktuellsten Stand. Das Plugin-Verzeichnis konnte nicht umbenannt werden. Unbekannter Update-Status "%s" WP HTTP Fehler:  eines Ordner mit dem Namen <span class=\"code\">%s</span> sein; nicht im Hauptverzeichnis der ZIP-Datei. wp_remote_get() meldet ein unerwartetes Ergebnis. 