#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-11-07 12:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: update/plugin-update-checker.php:230
#, php-format
msgid "The URL %s does not point to a valid plugin metadata file. "
msgstr ""

#: update/plugin-update-checker.php:232
msgid "WP HTTP error: "
msgstr ""

#: update/plugin-update-checker.php:234
msgid "HTTP response code is "
msgstr ""

#: update/plugin-update-checker.php:234
msgid " (expected: 200)"
msgstr ""

#: update/plugin-update-checker.php:236
msgid "wp_remote_get() returned an unexpected result."
msgstr ""

#: update/plugin-update-checker.php:281
#, php-format
msgid ""
"Can't to read the Version header for '%s'. The filename is incorrect or is "
"not a plugin."
msgstr ""

#: update/plugin-update-checker.php:302
#, php-format
msgid "Can't to read the plugin header for '%s'. The file does not exist."
msgstr ""

#: update/plugin-update-checker.php:329
#, php-format
msgid "Skipping update check for %s - installed version unknown."
msgstr ""

#: update/plugin-update-checker.php:607
msgid ""
"The directory structure of the update is incorrect. All plugin files should "
"be inside "
msgstr ""

#: update/plugin-update-checker.php:608
#, php-format
msgid ""
"a directory named <span class=\"code\">%s</span>, not at the root of the ZIP "
"file."
msgstr ""

#: update/plugin-update-checker.php:616
#, php-format
msgid "Renaming %s to %s&#8230;"
msgstr ""

#: update/plugin-update-checker.php:622
msgid "Plugin directory successfully renamed."
msgstr ""

#: update/plugin-update-checker.php:627
msgid "Unable to rename the update to match the existing plugin directory."
msgstr ""

#: update/plugin-update-checker.php:690
msgid "Check for updates"
msgstr ""

#: update/plugin-update-checker.php:734
msgid "This plugin is up to date."
msgstr ""

#: update/plugin-update-checker.php:736
msgid "A new version of this plugin is available."
msgstr ""

#: update/plugin-update-checker.php:738
#, php-format
msgid "Unknown update checker status \"%s\""
msgstr ""

#: update/plugin-update-checker.php:909
msgid ""
"Failed to parse plugin metadata. Try validating your .json file with http:"
"//jsonlint.com/"
msgstr ""

#: update/plugin-update-checker.php:921
msgid ""
"The plugin metadata file does not contain the required 'name' and/or "
"'version' keys."
msgstr ""
