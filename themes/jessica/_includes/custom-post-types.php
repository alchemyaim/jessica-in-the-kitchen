<?php
/**
 * Register [shop]
 */
function aa_register_shop() {
    $labels = array(
        'name' => _x( 'Shop', 'shop' ),
        'singular_name' => _x( 'Shop', 'shop' ),
        'add_new' => _x( 'Add New', 'shop' ),
        'add_new_item' => _x( 'Add New Shop', 'shop' ),
        'edit_item' => _x( 'Edit Shop', 'shop' ),
        'new_item' => _x( 'New Shop', 'shop' ),
        'view_item' => _x( 'View Shop', 'shop' ),
        'search_items' => _x( 'Search Shop', 'shop' ),
        'not_found' => _x( 'No Shop found', 'shop' ),
        'not_found_in_trash' => _x( 'No Shop found in Trash', 'shop' ),
        'parent_item_colon' => _x( 'Parent Shop:', 'shop' ),
        'menu_name' => _x( 'Shop', 'shop' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,

        'supports' => array( 'title' ),

        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-welcome-widgets-menus',

        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'shop', $args );
}
add_action( 'init', 'aa_register_shop' );


add_action( 'init', 'create_productcategory_hierarchical_taxonomy', 0 );
function create_productcategory_hierarchical_taxonomy() {
  $labels = array(
    'name' => _x( 'Product Category', 'taxonomy general name' ),
    'singular_name' => _x( 'Product Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Product Categories' ),
    'all_items' => __( 'All Product Categories' ),
    'parent_item' => __( 'Parent Product Category' ),
    'parent_item_colon' => __( 'Parent Product Category:' ),
    'edit_item' => __( 'Edit Product Category' ), 
    'update_item' => __( 'Update Product Category' ),
    'add_new_item' => __( 'Add New Product Category' ),
    'new_item_name' => __( 'New Product Category' ),
    'menu_name' => __( 'Product Categories' ),
  ); 	
  register_taxonomy('productcategory',array('shop'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'product-category' ),
  ));

}