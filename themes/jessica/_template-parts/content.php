<?php
/**
 * Template part for displaying posts.
 *
 */
?>

	<div class="catpost">
		<a href="<?php the_permalink(); ?>">
			<div class="imgbox">
				<?php the_post_thumbnail(array(655, 985)); ?>
				<span>View Post</span>
			</div>
			<h2><?php the_title(); ?></h2>
		</a>
	</div>
