<?php
/**
 * Template Name: Home Page
 *
 */ get_header();
?>


<div class="home-post section">
	<div class="wrapper">
		<?php global $post, $posts, $query_string;
		$m="";
		$m = new WP_Query('post_type=post&posts_per_page=1&orderby=date&order=DESC');
		if ( $m->have_posts() ) while ( $m->have_posts() ) : $m->the_post();
		?>
		<div class="postimgcol">
		<?php if ( get_field('post_primary_img1') && get_field('post_primary_img2')) : ?>
			<a class="swapimgs" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php
				$image_object = get_field('post_primary_img1');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
				<?php
				$image_object = get_field('post_primary_img2');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
			</a>
		<?php else: ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>			
		<?php endif; ?>
		</div>
		<div class="posttxtcol">
			<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="postcats">
				<?php $categories = get_the_category();
				$separator = ' ›› ';
				$output = '';
				if ( ! empty( $categories ) ) {
				    foreach( $categories as $category ) {
				        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
				    }
				    echo trim( $output, $separator );
				} ?>
			</div>
			<?php if(get_field('post_custom_excerpt')): ?>
			<div class="excerpt"><?php the_field('post_custom_excerpt'); ?></div>
			<?php else: ?>
			<div class="excerpt"><?php the_excerpt(''); ?></div>
			<?php endif; ?>
			<div class="recipe-meta">
				<?php if(get_field('recipe_preparation')): ?><div>Preparation: <?php the_field('recipe_preparation'); ?></div><?php endif; ?>
				<?php if(get_field('recipe_cook_time')): ?><div>Cook Time: <?php the_field('recipe_cook_time'); ?></div><?php endif; ?>
				<?php if(get_field('recipe_serves')): ?><div>Serves: <?php the_field('recipe_serves'); ?></div><?php endif; ?>
			</div>
			<a class="view-post" href="<?php the_permalink(); ?>">View the Post</a>

	<?php if(have_rows('recipe_ingredients')): ?>
	<div class="ingredients">
		<div class="ingr-title">Ingredients</div>
		<?php while(have_rows('recipe_ingredients')): the_row(); ?>
			<div><strong><?php the_sub_field('ingredient_title'); ?></strong></div>
			<?php if(have_rows('ingredient_list')): ?>
			<ul class="ingredient-list">
				<?php while(have_rows('ingredient_list')): the_row(); ?>
				<li><?php the_sub_field('ingredient_list_text'); ?></li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>


		</div>
	</div>

<?php endwhile; wp_reset_query(); ?>
	</div>
</div>


<div class="homeshop section">
	<div class="wrapper">
		<div class="homeshoptitle"><?php the_field('home_shop_title', 'option'); ?></div>
		<?php $term = get_field('home_shop_link', 'option'); ?>
		<a class="homeshoplink" href="<?php echo home_url(); ?>/product-category/<?php echo $term->slug; ?>"><?php the_field('home_shop_link_text', 'option'); ?></a>
		<?php if(have_rows('home_shop_ftrd', 'option')): ?>
		<div class="homeshoplist section">
			<?php while(have_rows('home_shop_ftrd', 'option')): the_row(); ?>
			<div>
			<?php
			$post_object = get_sub_field('home_shop_ftrd_item');
			if( $post_object ): 
			$post = $post_object;
			setup_postdata( $post ); ?>
			    <a href="<?php the_field('shop_prod_link'); ?>">
					<?php
					$image_object = get_field('shop_prod_photo');
					$image_size = 'primaryimg';
					$image_url = $image_object['sizes'][$image_size];
					?>
					<img src="<?php echo $image_url; ?>">
			    </a>
			    <?php wp_reset_postdata(); ?>
			<?php endif; ?>
	
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</div>

<div class="home-season-loved">
	<div class="wrapper">

		<div class="home-season-toggle"></div>
		<div class="home-season-loved-slide home-season-slide owl-carousel">
			<div class="inseason">
			<?php
			$ftcount = get_field('home_inseason_ft', 'option');
			$count = 0;
			$your_repeater = get_field('home_inseason', 'option');
			if($your_repeater){
			   while( have_rows('home_inseason', 'option') ): the_row();
			   $count++; ?>
						<?php
						$post_object = get_sub_field('home_inseason_post');
						if( $post_object ): 
						$post = $post_object;
						setup_postdata( $post ); ?>
				
				<?php if ($count == $ftcount) : ?>
			
					<span class="hsl-image">
					<?php if ( get_field('post_primary_img1')) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php
							$image_object = get_field('post_primary_img1');
							$image_size = 'primaryimg';
							$image_url = $image_object['sizes'][$image_size];
							?>
							<img src="<?php echo $image_url; ?>">
						</a>
					<?php else: ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail(); ?>
						</a>			
					<?php endif; ?>
					</span>
			
			
			
				<?php endif; ?>
			
			
						    <?php wp_reset_postdata(); ?>
						<?php endif; ?>
			
			<?php endwhile;  
			}
			?>
			<?php
			$ftcount = get_field('home_inseason_ft', 'option');
			$count = 0;
			echo $ftcounttt;
			$your_repeater = get_field('home_inseason', 'option');
			if($your_repeater): ?>
				<div class="hsl-titles">
			   <?php while( have_rows('home_inseason', 'option') ): the_row();
			   $count++; ?>
						<?php
						$post_object = get_sub_field('home_inseason_post');
						if( $post_object ): 
						$post = $post_object;
						setup_postdata( $post ); ?>
				
				<div class="hsl-title <?php if ($count == $ftcount) : ?>featured<?php endif; ?>">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<span><?php echo '0'. $count; ?></span>
						<h2><?php the_title(); ?></h2>
					</a>
				</div>
			
					    <?php wp_reset_postdata(); ?>
					<?php endif; ?>
			<?php endwhile;  ?>
			</div>
			
			<?php endif; ?>
			<div class="inseasonwhite"></div>
		</div>

			<div class="mostloved">
			<?php
			$ftcount = get_field('home_mostloved_ft', 'option');
			$count = 0;
			$your_repeater = get_field('home_mostloved', 'option');
			if($your_repeater){
			   while( have_rows('home_mostloved', 'option') ): the_row();
			   $count++; ?>
						<?php
						$post_object = get_sub_field('home_mostloved_post');
						if( $post_object ): 
						$post = $post_object;
						setup_postdata( $post ); ?>
				
				<?php if ($count == $ftcount) : ?>
			
					<span class="hsl-image">
					<?php if ( get_field('post_primary_img1')) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php
							$image_object = get_field('post_primary_img1');
							$image_size = 'primaryimg';
							$image_url = $image_object['sizes'][$image_size];
							?>
							<img src="<?php echo $image_url; ?>">
						</a>
					<?php else: ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail(); ?>
						</a>			
					<?php endif; ?>
					</span>
			
			
			
				<?php endif; ?>
			
			
						    <?php wp_reset_postdata(); ?>
						<?php endif; ?>
			
			<?php endwhile;  
			}
			?>
			<?php
			$ftcount = get_field('home_mostloved_ft', 'option');
			$count = 0;
			echo $ftcounttt;
			$your_repeater = get_field('home_mostloved', 'option');
			if($your_repeater): ?>
				<div class="hsl-titles">
			   <?php while( have_rows('home_mostloved', 'option') ): the_row();
			   $count++; ?>
						<?php
						$post_object = get_sub_field('home_mostloved_post');
						if( $post_object ): 
						$post = $post_object;
						setup_postdata( $post ); ?>
				
				<div class="hsl-title <?php if ($count == $ftcount) : ?>featured<?php endif; ?>">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<span><?php echo '0'. $count; ?></span>
						<h2><?php the_title(); ?></h2>
					</a>
				</div>
			
					    <?php wp_reset_postdata(); ?>
					<?php endif; ?>
			<?php endwhile;  ?>
			</div>
			
			<?php endif; ?>
			<div class="mostlovedwhite"></div>
		</div>

	</div>
	</div>

</div>

<div class="home-post section">
	<div class="wrapper">
		<?php global $post, $posts, $query_string;
		$m="";
		$m = new WP_Query('post_type=post&posts_per_page=1&orderby=date&order=DESC&offset=1');
		if ( $m->have_posts() ) while ( $m->have_posts() ) : $m->the_post();
		?>
		<div class="postimgcol">
		<?php if ( get_field('post_primary_img1') && get_field('post_primary_img2')) : ?>
			<a class="swapimgs" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php
				$image_object = get_field('post_primary_img1');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
				<?php
				$image_object = get_field('post_primary_img2');
				$image_size = 'primaryimg';
				$image_url = $image_object['sizes'][$image_size];
				?>
				<img src="<?php echo $image_url; ?>">
			</a>
		<?php else: ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>			
		<?php endif; ?>
		</div>
		<div class="posttxtcol">
			<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="postcats">
				<?php $categories = get_the_category();
				$separator = ' ›› ';
				$output = '';
				if ( ! empty( $categories ) ) {
				    foreach( $categories as $category ) {
				        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
				    }
				    echo trim( $output, $separator );
				} ?>
			</div>
			<?php if(get_field('post_custom_excerpt')): ?>
			<div class="excerpt"><?php the_field('post_custom_excerpt'); ?></div>
			<?php else: ?>
			<div class="excerpt"><?php the_excerpt(''); ?></div>
			<?php endif; ?>
			<div class="recipe-meta">
				<?php if(get_field('recipe_preparation')): ?><div>Preparation: <?php the_field('recipe_preparation'); ?></div><?php endif; ?>
				<?php if(get_field('recipe_cook_time')): ?><div>Cook Time: <?php the_field('recipe_cook_time'); ?></div><?php endif; ?>
				<?php if(get_field('recipe_serves')): ?><div>Serves: <?php the_field('recipe_serves'); ?></div><?php endif; ?>
			</div>
			<a class="view-post" href="<?php the_permalink(); ?>">View the Post</a>

	<?php if(have_rows('recipe_ingredients')): ?>
	<div class="ingredients">
		<div class="ingr-title">Ingredients</div>
		<?php while(have_rows('recipe_ingredients')): the_row(); ?>
			<div><strong><?php the_sub_field('ingredient_title'); ?></strong></div>
			<?php if(have_rows('ingredient_list')): ?>
			<ul class="ingredient-list">
				<?php while(have_rows('ingredient_list')): the_row(); ?>
				<li><?php the_sub_field('ingredient_list_text'); ?></li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>


		</div>
	</div>

<?php endwhile; wp_reset_query(); ?>
	</div>
</div>

<div class="home-post homeftvideo section">
	<div class="wrapper">
		<div class="section">
			<?php
			$post_object = get_field('home_featured_video', 'option');
			if( $post_object ): 
			$post = $post_object;
			setup_postdata( $post ); ?>
			<div class="postimgcol">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>			
			</div>
			<div class="posttxtcol">
				<div class="fttag">Featured Video</div>
				<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="postcats">
					<?php $categories = get_the_category();
					$separator = ' ›› ';
					$output = '';
					if ( ! empty( $categories ) ) {
					    foreach( $categories as $category ) {
					        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
					    }
					    echo trim( $output, $separator );
					} ?>
				</div>
				<?php if(get_field('post_custom_excerpt')): ?>
				<div class="excerpt"><?php the_field('post_custom_excerpt'); ?></div>
				<?php else: ?>
				<div class="excerpt"><?php the_excerpt(''); ?></div>
				<?php endif; ?>
				<a class="view-post" href="<?php the_permalink(); ?>">View the Post</a>
			</div>
	
		    <?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="homesubscribe section">
	<div class="wrapper">
		<h5><?php the_field('home_subscribe_title', 'option'); ?></h5>
		<p><?php the_field('home_subscribe_sub_title', 'option'); ?></p>
		<form action="//jessicainthekitchen.us2.list-manage.com/subscribe/post?u=72a1d133146a6587d7e5e0328&amp;id=<?php the_field('home_subscribe_list_id', 'option'); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			
			<input type="email" value="" name="EMAIL" placeholder="enter your email address" class="required email" id="mce-EMAIL">
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_72a1d133146a6587d7e5e0328_<?php the_field('home_subscribe_list_id', 'option'); ?>" tabindex="-1" value=""></div>
		    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
			<div id="mce-responses" >
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>
		</form>
	</div>
	<img class="large-icon" src="<?php the_field('main_icon', 'options'); ?>">
</div>

<?php echo do_shortcode( '[ajax_load_more container_type="div" css_classes="mainloadmore" post_type="post" posts_per_page="1" offset="2" scroll="false" button_label="+ Load more Posts"]' ); ?>



<?php get_footer(); ?>
